from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Pergunta, Opcao



class IndexView(generic.ListView):
    context_object_name = 'ultimas_perguntas'
    template_name = 'main/index.html'
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
            ).order_by('-data_publicacao')[:5]



class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'main/detalhes.html'
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())



class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'main/resultado.html'




def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'main/detalhes.html', {'pergunta': pergunta,'error_message': "Selecione uma opcão válida"})
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(
            reverse('main:resultado', args=(pergunta.id,)))
